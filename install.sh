#!/bin/bash
# GPL v3. Copyright (C) 2019 (energy1011[4t)gmail(d0t]com)
#This script installs artisans-fzf inside a project folder and check dependencies
#Usage: sudo ./install /home/<your_user>/myLaravelProject"

INSTALL_PATH=$1
PROGRAM_NAME="Artisans-fzf"
PROGRAM_FILENAME="artisans"
CONF_FILENAME_EXAMPLE=".fzf-artisans.conf"
DOT_HIST_FILENAME=".fzf-artisans-histfile"


function create_conf_file(){
    cat << EOF > $CONF_FILENAME_EXAMPLE
{
  "HOST_API_ARTISANS":"",
  "DOT_HIST_FILENAME": "$DOT_HIST_FILENAME",
  "DOT_HIST_FILE_LIMIT": 50
}
EOF
}

function check_correct_path(){
    #Check path in arg $1
    if [ -z "$1" ]; then
        echo "[Error] Path to folder needed: sudo ./install <path_to_project>"
        echo "[Help] Usage example: sudo ./install /home/<your_user>/myLaravelProject"
        exit 1
    fi

    #Check for correct path
    if [ ! -d "$1" ]; then
        echo "[Error] Incorrect path $1"
        exit 1
    fi

    #Check if ends with '/'
    if [[ ! $INSTALL_PATH =~ '/'$ ]]; then
        INSTALL_PATH=$INSTALL_PATH"/"
    fi
}


function show_installer_info(){
    echo "[i] Installer info";
    echo "Program name: $PROGRAM_NAME";
    echo "Install path: $INSTALL_PATH";
    echo "---------------------------";
    echo ""
}

function check_for_root(){
    # #Check run as root
    if [[ $EUID -ne 0 ]]; then
        echo "[Info] This script must be run as root"
        echo "[Info] run like: sudo ./install.sh <path_to_project>"
        exit 1
    fi
}

function install_dependencies(){

    # check pip3
    pip3 -v >& /dev/null
    if [ $? -ne 1 ]; then
        apt -y install python3-pip
        pip3 install --upgrade pip3
    fi

    # install python modules
    sudo -H pip3 install -r requirements.txt

    # Check for fzf
    fzf --version >& /dev/null
    if [ $? -ne 1 ]; then
        git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
        ~/.fzf/install --key-bindings --completion --update-rc
    fi
}

function cp_file(){
    origin=$1
    dest=$2
    #copy PROGRAM_FILENAME to INSTALL_PATH
    cp -pf $origin $dest;
    if [ $? -ne 0 ]; then
        echo "[$PROGRAM_NAME]: Error copying $origin to $dest"
    fi
}

function copy_files(){
    #copy PROGRAM_FILENAME to INSTALL_PATH
    cp_file $PROGRAM_FILENAME "$INSTALL_PATH$PROGRAM_FILENAME"
    #if CONF_FILENAME_EXAMPLE overwrite
    echo "checking:"$INSTALL_PATH$CONF_FILENAME_EXAMPLE

    if [ -f "$INSTALL_PATH$CONF_FILENAME_EXAMPLE" ]; then
        echo "File $INSTALL_PATH$CONF_FILENAME_EXAMPLE already exists. It's recommended to backup it first before overwrite."
        echo -e "[?] Do you want to overwrite (y/n)?"
        read -n1 optOverwrite
        echo ""
        if [ "$optOverwrite" == "y" ]; then
            echo "[Info] Overwrite done."
            #copy CONF_FILENAME_EXAMPLE
            cp_file $CONF_FILENAME_EXAMPLE "$INSTALL_PATH$CONF_FILENAME_EXAMPLE"
        fi
    else
        #copy CONF_FILENAME_EXAMPLE
        cp_file $CONF_FILENAME_EXAMPLE "$INSTALL_PATH$CONF_FILENAME_EXAMPLE"
    fi
}

function main(){
    path=$1
    check_correct_path $path
    show_installer_info
    check_for_root
    install_dependencies
    create_conf_file
    copy_files
    echo "[$PROGRAM_NAME]: Script finished OK, enjoy.";
    echo "[Info] Go to $1 and run: ./$PROGRAM_FILENAME"
    exit 0
}

main $1
