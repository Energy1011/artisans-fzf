![Artisans-fzf](https://gitlab.com/Energy1011/artisans-fzf/raw/master/artisans-fzf.png)

## Artisans-fzf
This script is used with [fzf project](https://github.com/junegunn/fzf) to manage artisan commands in laravel projects, even getting data from your API in the API mode.

## Installation
Clone the project:
```bash
git clone https://gitlab.com/Energy1011/artisans-fzf.git
```
Enter the artisans-fzf folder:
```bash
cd artisans-fzf
```
Once the project is cloned, run:
```bash
sudo ./install.sh /your/laravel/project/path
```

## Usage by default: *On the fly mode*
Once installed, type './artisans' inside your laravel project folder, now you can select items from `artisan list` Thanks to [fzf project](https://github.com/junegunn/fzf), I called this mode: *On the fly mode* in the artisans-fzf context to make a diference between API mode too explained following.

## API mode
You can make your own API in laravel and use artisans-fzf to select commands, resources, models or whatever you want. Initially, I created artisans-fzf for large projects, I needed select models from a huge list of namespacing/models that I can request to the project's API. No matter what you want to retrive from your API (models, list of users, records, whatever..), artisans-fzf can launch commands combined with your selected API data.

### How to set API mode ?
Open your .fzf-artisans.conf file in your project and setup the **HOST_API_ARTISANS** variable with your API's url for example:
```json
{
  "HOST_API_ARTISANS":" http://myproject.test/api/foo/",
  ...
  .
}

```


## How to use history mode ?
Call to artisans-fzf with either the following options:
```bash
./artisans -l
#or
./artisans --history
```
history commands are saved and setted at *"DOT_HIST_FILENAME": ".fzf-artisans-histfile"* in the .fzf-artisans.conf

**RECOMENDATION:** add the .fzf-artisans-histfile to your .gitignore

Enjoy.
